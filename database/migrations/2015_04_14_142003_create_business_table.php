<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('business', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('slug', 200)->index();
			$table->string('name', 200);
			$table->text('description')->nullable();
			$table->double('latitude', 9, 6)->nullable();
			$table->double('longitude', 9, 6)->nullable();
			$table->string('phone', 50)->nullable();
			$table->string('address', 250)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('business');
	}

}
