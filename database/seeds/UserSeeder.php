<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserSeeder extends Seeder {

	public function run()
	{
		$user = new \App\User;

		$user->name = 'Administrator';
		$user->email = 'admin@localsuburb.com';
		$user->password = bcrypt('password');

		$user->save();
	}

}