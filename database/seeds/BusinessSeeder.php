<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BusinessSeeder extends Seeder {

	public function run()
	{
		DB::table('business')->truncate();

		$business = [
			'name' => 'SEO Tweaks',
			'website' => 'www.seotweaks.com',
			'slug' => $this->slugify('SEO Tweaks'),
			'phone' => '123456',
			'email' => 'admin@seotweaks.com',
			'photo' => 'uploads/images/d7f84531f62ccdd9b691e187ebd13d70.png',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean maximus feugiat ante, et tincidunt libero euismod ut. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae',
			'address' => '25th Jimboomba Street 29388'
		];

		\App\Models\Business::insert($business);
		
	}

	protected function slugify($text)
	{
		$text = str_replace(', ', ',-', $text);

		return strtolower( str_replace(' ', '-', $text) );
	}
}