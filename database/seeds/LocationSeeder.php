<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class LocationSeeder extends Seeder {

	public function run()
	{
		DB::table('locations')->truncate();

		$locations = [
			'Jimboomba, Queensland, Australia'
		];

		$data = [];

		foreach ($locations as $location) {
			array_push($data, [
				'slug' 		=> $this->slugify($location),
				'location' 	=> $location
			]);
		}
		

		App\Models\Location::insert($data);
	}

	protected function slugify($text)
	{
		$text = str_replace(', ', ',', $text);

		return strtolower( str_replace(' ', '-', $text) );
	}
}