<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ListingSeeder extends Seeder {

	public function run() {
		DB::table('listings')->truncate();

		$o = [
			'category_id' => \App\Models\Category::where('category', 'Software Company')->first()->id,
			'business_id' => \App\Models\Business::where('name', 'SEO Tweaks')->first()->id,
			'location_id' => \App\Models\Location::where('location', 'Jimboomba, Queensland, Australia')->first()->id,
		];

		App\Models\Listing::insert($o);
	}
}