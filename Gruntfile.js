module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    pub: 'public/app/',
    bower: 'bower_components/',
    concat: {
      angularjs: {
        options: {
          options: {
            stripBanners: true,
            banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
              '<%= grunt.template.today("yyyy-mm-dd") %> */',
          },
        },
        files: {
          '<%= pub %>assets/js/angular.packed.min.js': [
            'bower_components/angular/angular.min.js',
            'bower_components/angular-route/angular-route.min.js',
            'bower_components/angular-ui/build/angular-ui.min.js',
            'bower_components/angular-ui-router/release/angular-ui-router.min.js',
            'bower_components/angular-material/angular-material.min.js',
            'bower_components/angular-animate/angular-animate.min.js',
            'bower_components/angular-aria/angular-aria.min.js',
            'bower_components/angular-local-storage/dist/angular-local-storage.min.js',
            'bower_components/angular-messages/angular-messages.min.js',
            'bower_components/angular-ui-bootstrap-bower/ui-bootstrap.min.js'          ]
        },
      },
      localsuburb: {
        files: {
          '<%= pub %>assets/js/src/main.js': [
            '<%= pub %>app.js',
            '<%= pub %>env.js',
            '<%= pub %>router.js',
            '<%= pub %>factories/*.js',
            '<%= pub %>services/*.js',
            '<%= pub %>controllers/*.js',
          ]
        }
      },
      all: {
        files: {
          '<%= pub %>assets/js/app.js': [
            '<%= pub %>assets/js/src/main.js'
          ]
        }
      },
      frontVendor: {
        files: {
          'public/app/assets/css/vendor.css': [
            'public/app/assets/css/src/animate.css',
            'public/app/assets/css/src/font-awesome.css',
            'public/app/assets/css/src/angular-material.css',
            'public/app/assets/css/src/bootstrap.css',
          ]
        }
      },
      frontCSS: {
        files: {
          'public/app/assets/css/app.css': [
            'public/app/assets/css/src/main.css'
          ]
        }
      }
    },
    uglify: {
        options: {
          compress: {
            drop_console: true
          }
        },
        dist: {
          files: {
            '<%= pub %>assets/js/app.min.js': ['<%= pub %>assets/js/app.js']
          }
        },
        dev: {
          files: {
            '<%= pub %>assets/js/app.min.js': ['<%= pub %>assets/js/app.js'],

          }
        }
    },
    cssmin: {
      options: {
        keepSpecialComments: 0
      },
      front: {
        files: {
          'public/app/assets/css/app.min.css': [
            'public/app/assets/css/app.css'
          ],
          'public/app/assets/css/vendor.min.css': [
            'public/app/assets/css/vendor.css'
          ]
        }
      },
    },
    watch: {
      js: {
        options: {
          livereload: true
        },
        files: [
          '<%= pub %>*.js',
          '<%= pub %>controllers/*.js',
          '<%= pub %>services/*.js', 
        ],
        tasks: ['concat', 'uglify:dev']
      },
      html: {
        options: {
          livereload: true
        },
        files: [
          '<%= pub %>views/**/*.html'
        ]
      },
      css: {
        options: {
          livereload: true
        },
        files: [
          '<%= pub %>assets/sass/**/*.scss'
        ],
        tasks: ['sass', 'concat:frontCSS', 'cssmin']
      }
    },
    bowercopy: {
      vendors: {
        options: {
          destPrefix: 'public/app/assets'
        },
        files: {
          'js/src/bootstrap.min.js': 'bootstrap/dist/js/bootstrap.min.js',
          'css/src/bootstrap.css': 'bootstrap/dist/css/bootstrap.css',
          'css/src/animate.css': 'animate.css/animate.css',
          'js/src': 'jquery/dist',
          'css/src': 'font-awesome/css',
          'fonts': 'font-awesome/fonts',
          'css/src/angular-material.css': 'angular-material/angular-material.css',
          'js/src/jquery-ui.min.js': 'jquery-ui/jquery-ui.min.js'
        }
      }
    },
    sass: {
      dist: {
        options: {
          style: 'expanded'
        },
        files: {
          '<%= pub %>assets/css/src/main.css': '<%= pub %>assets/sass/main.scss',
        }
      }
    },
    clean: {
      all: {
        src: [
          "!dist/.gitignore",
          "dist/**"
        ]
      },
      dist: {
        src: [
          "dist/storage/framework/cache/*",
          "dist/storage/framework/sessions/*",
          "dist/storage/framework/views/*",
          "dist/storage/logs/*.log",
          "dist/vendor/**/*.txt",
          "dist/vendor/**/*.md",
          "dist/vendor/**/*.json",
          "dist/vendor/**/*.xml",
          "dist/vendor/**/*.xml.dist",
          "dist/vendor/**/doc",
          "dist/vendor/**/tests",
          "dist/vendor/**/test",
          "dist/vendor/**/Tests",
          "dist/vendor/**/Test",
          "dist/public/app/assets/css/src",
          "dist/vendor/**/lang/*.php",
          "!dist/vendor/**/lang/en.php",
          "dist/vendor/**/Lang/*.php",
          "!dist/vendor/**/Lang/en.php",
        ]
      }
    },
    copy: {
      bootstrap: {
        files: [
          {
            expand: true,
            cwd: 'bower_components/bootstrap/fonts/',
            src: [
              '**'
            ],
            dest: 'public/app/assets/fonts'
          }
        ]
      },
      dist: {
        files: [
          {
            expand: true, 
            src: [
              'vendor/**',
              'app/**',
              'bootstrap/**',
              'config/**',
              'database/**',
              'public/**',
              'resources/**',
              'storage/**',
              'test/**',
              '.env',
              '.htaccess',
              'index.php',
              'composer.json',
              'composer.lock'
            ],
            dest: 'dist/'},
        ]
      }
    },
    chmod: {
      options: {
        mode: '777'
      },
      storage: {
        src: [
          'dist/storage/**',
          'dist/public/uploads/**'
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-bowercopy');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-chmod');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  grunt.registerTask('default', ['bowercopy', 'copy:bootstrap', 'sass', 'concat', 'uglify', 'cssmin']);
  grunt.registerTask('dev', ['sass', 'concat', 'uglify', 'watch']);
  grunt.registerTask('dist', ['default', 'clean:all', 'copy:dist', 'clean:dist', 'chmod']);

};