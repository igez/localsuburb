<?php

class APITest extends TestCase {

	public function setUp()
	{
		parent::setUp();
	 
		$this->mock = $this->mock('App\Controllers\APIController');
	}

	public function mock($class)
	{
	  	$mock = Mockery::mock($class);
	 
	  	$this->app->instance($class, $mock);
	 
	  	return $mock;
	}

	public function tearDown()
	{
		Mockery::close();
	}

    public function testSomethingIsTrue()
    {
        $this->call('GET', '/api');

    	$this->assertResponseOk();
    }

    public function testAPICategory()
    {

    	$category = new \App\Models\Category;

    	$category->where('category', 'like', 'ab%');

    	$this->call('GET', 'api/category', ['s' => 'ab']);

    	$response = [
    		'count' => 1,
    		'results' => $category->get()
    	];

    	$this->assertResponseOk();
    	$this->assertJson(json_encode($response));
    }

    public function testListing()
    {
    	$response = [
    		'error' => 0,
    		'count' => 0,
    		'results' => []
    	];

    	$this->call('GET', 'api/listing', ['type' => 'Bakery', 'location' => 'test']);

    	$this->assertResponseOk();
    	$this->assertJson(json_encode($response));
    }

    public function testGuestAccess()
    {
    	$this->call('GET', '/admin');
    	$this->assertSessionHas('message', 'You\'re not authorized to access the page, please login.');
    	$this->assertRedirectedTo('account/login');
    }

    public function testLoginFailure()
    {
    	Session::start();
    	$credentials = array(
	        'email'=>'admin',
	        'password'=>'admin',
	        '_token' => csrf_token()
		);

		$response = $this->call('POST', 'account/login', $credentials);

		$this->assertSessionHas('message', 'Invalid Email or Password');
		$this->assertRedirectedTo('account/login');
    }

    public function testLoginSuccess()
    {
    	Session::start();
    	$credentials = array(
	        'email'=>'admin@admin.com',
	        'password'=>'password',
	        '_token' => csrf_token()
		);

		$response = $this->call('POST', 'account/login', $credentials);

		$this->assertRedirectedTo('admin');
		$this->assertEquals(Auth::user()->email, 'admin@admin.com');
		Auth::logout();
		$this->assertFalse(Auth::user() instanceof Illuminate\Database\Eloquent\Model);
    }

    public function testUserCreation()
    {
    	$user = new \App\User;

    	$user->email = 'test@test.com';
    	$user->password = bcrypt('password');

    	$user->save();

    	$this->assertEquals($user->email, 'test@test.com');
    	Session::start();
    	$credentials = array(
	        'email'		=> $user->email,
	        'password'	=> 'password',
	        '_token' => csrf_token()
		);

		$response = $this->call('POST', 'account/login', $credentials);

		$this->assertRedirectedTo('admin');
		$this->assertEquals(Auth::user()->email, 'test@test.com');

		$this->call('GET', 'account/logout');

		$this->assertRedirectedTo('account/login');

    	$user->delete();

    }

}