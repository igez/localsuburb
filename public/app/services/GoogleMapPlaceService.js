angular
.module('GoogleMapPlaceService', [])
.service('PlaceService', ['$q', '$http', function($q, $http) {
	var Result = [],
		Service = {
			type		: ['(regions)'],
			restrictions: {country: 'aus'}
		}

	// this.getSearchResult = function(keyword) {
	// 	var service = new google.maps.places.AutocompleteService();
	// 	var deferred = $q.defer();
	// 	var filterCountry = function(arr) {
	// 		var AllowedCountries = ['Australia', 'United States', 'United Kingdom', 'Ireland'],
	// 			exist = false;

	// 		angular.forEach(arr, function(val, key) {
	// 			if ( AllowedCountries.indexOf(val.value) !== -1 )
	// 				exist = true;
	// 		});
	// 		return exist;
	// 	}

	// 	Result = [];

	// 	service.getPlacePredictions({ 
	// 		input: keyword, 
	// 		types: Service.type, 
	// 		// componentRestrictions: Service.restrictions 
	// 	}, function(predictions, status) {
	// 		angular.forEach(predictions, function(value, key){
	// 			if (value.terms.length > 2)
	// 				if ( filterCountry(value.terms) )
	// 					Result.push( {id: value.place_id, value: value.description } );
	// 		});
	// 		deferred.resolve( (Result.length) ? Result : false );
	// 	});

	// 	return deferred.promise;
	// }

	this.getSearchResult = function(val) {
		var deferred = $q.defer(),
			$results = [];

		$http
		.get('/api/suburb?search=' + val)
		.success(function(results) {
			if (results.length) {
				angular.forEach(results, function(val, idx) {
					$results.push({ key: val.id, value: val.result });

					deferred.resolve($results);
				});
			}
			else {
				deferred.resolve($results);
			}
		});

		return deferred.promise;
	}
}]);