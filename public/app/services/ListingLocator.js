angular
.module('LocalSuburb.Service.Listing', [])
.service('$listingLocator', ['$http', '$q', function($http, $q) {

	this.find = function(params) {
		var deferred = $q.defer();

		window.setTimeout(function() {
			$http
			.get('api/listing?type=' + params.category + '&location=' + params.location)
			.success(function(data) {
				deferred.resolve(data);
			})
			.error(function(xhr) {
				deferred.reject(xhr);
			});
		}, 500);

		return deferred.promise;


	}

}]);