angular
.module('LocalSuburb.Service.Auth', [])
.service('$authService', ['localStorageService', '$log', '$http', '$q', function(localStorageService, $log, $http, $q) {

	var _ = this;

	this.isLoggedIn = function() {

		return ( localStorageService.get('u') && localStorageService.get('t') ) ? true : false;
	}

	this.login = function($data) {
		var deferred = $q.defer();
		setTimeout(function() {
			$http
			.post('/api/auth/login', $data)
			.then(function(data) {
				if (!data.data.error) {
					_.setLoggedUser(data.data.metadata);
					_.setToken(data.data.token);
				}
				deferred.resolve(data);
			})
		}, 2000);

		return deferred.promise;
	}

	this.logout = function()  {
		localStorageService.clearAll();

		return true;
	}

	this.setLoggedUser = function(val) {
		localStorageService.set('u', btoa(val));
	}

	this.setToken = function(token) {
		localStorageService.set('t', token);
	}

	this.register = function(data) {
		var deferred = $q.defer();

		$http
		.post('/api/auth/register', data)
		.then(function(data) {
			deferred.resolve(data);
		});

		return deferred.promise;
	}

}]);