/**
 * Helper Service
 */
angular
.module('LocalSuburb.Service.helper', [])
.service('$helper', function() {

	var removeWhiteSpace = function(text) {
		if (text.match(/,\s/g))
			text = text.replace(/,\s/g, ",");

		return text.replace(/\s/g, "-");
	}

	this.slugify = function(text) {
		return removeWhiteSpace(text);
	}

});