angular
.module('LocalSuburb.Service.Category', [])
.service('CategoryService', ['$http', '$q', function($http, $q) {

    /**
     * @param value
     * @returns {*}
     */
    this.searchCategory = function(value) {

        var $result = [],
            deferred = $q.defer();

        $http
        .get('/api/category?s=' +  value)
        .success(function(data) {
            if (data.count > 0) {
                angular.forEach(data.results, function(val, idx) {
                    $result.push({ key: val.id, value: val.category });

                    deferred.resolve($result);
                });
            } else {
                deferred.resolve($result);
            }
        })
        .error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

}]);