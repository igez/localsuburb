(function() {
	function loadStyleSheet(src){
        if (document.createStyleSheet) document.createStyleSheet(src);
        else {
            var stylesheet = document.createElement('link');
            stylesheet.href = src;
            stylesheet.rel = 'stylesheet';
            stylesheet.type = 'text/css';
            document.getElementsByTagName('head')[0].appendChild(stylesheet);
        }
    }

    var css = [
    	'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300',
    	'public/app/assets/css/vendor.min.css',
    	'public/app/assets/css/app.min.css'
    ];

    for (var i=0; i<css.length; i++) {
    	loadStyleSheet(css[i]);
    }
})();
angular.module('LocalSuburb', [
	'ui.router',
	'LocalStorageModule',
	'ngMaterial',
	'ngMessages',
	'ngAnimate',
	'LocalSuburb.router',
	'GoogleMapPlaceService',
    'LocalSuburb.Service.Category',
    'LocalSuburb.Service.helper',
    'LocalSuburb.Service.Auth',
    'LocalSuburb.Service.Listing',
	'LocalSuburb.Controller.base',
	'LocalSuburb.Controller.index',
	'LocalSuburb.Controller.search',
	'LocalSuburb.Controller.listing',
	'LocalSuburb.Controller.AccountController',
])
.controller('SiteController', ['$scope', function($scope){
	$scope.site = {
		title: 'LocalSuburb'
	}

	$scope.ready = true;
}]);