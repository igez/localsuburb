angular
.module('LocalSuburb.router', [])
.config([
	'$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    'localStorageServiceProvider',
    function($stateProvider, $urlRouterProvider, $locationProvider, localStorageServiceProvider) {
    	$urlRouterProvider.when('', '/');
		
		$stateProvider
    		.state('root', {
                url         : '',
                templateUrl : 'public/app/views/master.html',
                controller  : 'BaseController'
            })
            .state('root.index', {
            	url 		: '/',
                templateUrl	: 'public/app/views/index.html',
                controller 	: 'IndexController',
            })
            .state('root.listing', {
                url         : '/free-listing',
                templateUrl : 'public/app/views/listing/new.html',
                controller  : 'ListingController',
            })
            .state('root.search', {
                url         : '/s/:category/:location',
                templateUrl : 'public/app/views/search.html',
                controller  : 'SearchController',
            })
            .state('root.login', {
                url         : '/login',
                templateUrl : 'public/app/views/account/login.html',
            })
            .state('root.register', {
                url         : '/register',
                templateUrl : 'public/app/views/account/register.html',
            })

        if(window.history && window.history.pushState){
            $locationProvider.html5Mode(true);
        }

        localStorageServiceProvider
        .setStorageType('cookieStorage')
        .setPrefix('LocalSuburb')
	}
])
.run(['$rootScope', '$state', '$stateParams', '$log', '$authService', 'localStorageService',
function($rootScope, $state, $stateParams, $log, $authService, localStorageService) {
    $rootScope.$on('$stateChangeStart', function(event, toState, toStateParams) {
        if (toState.name === 'root.listing') {
            if ( !$authService.isLoggedIn() ) {
                $rootScope.$broadcast('permissionDenied');
                event.preventDefault();
                localStorageService.set('b', toState.name);
                $state.go('root.login');
            }
        }
        if ( toState.name === 'root.login' ) {
            if ( $authService.isLoggedIn() ) {
                event.preventDefault();
                $state.go('root.index');
            }
        }
    });
}]);