/**
 * Listing Controller
 */
angular
.module('LocalSuburb.Controller.listing', ['ngAnimate'])
.controller('ListingController', [
'$scope',
'$q',
'$mdDialog',
'$state',
'$log',
'localStorageService',
'$authService',
'$http',
'PlaceService',
'CategoryService',
function($scope, $q, $mdDialog, $state, $log, localStorageService, $authService, $http, PlaceService, CategoryService) {

	$scope.iAgree = false;
	$scope.progress = false;

	var agreement = false;

	$scope.getAgreement = function() {
		return (localStorageService.get('a')) ? true : false;
	}

	$scope.setAgreement = function(value) {
		localStorageService.set('a', value);
	}

	$scope.showListingForm = function() {
		return $scope.getAgreement() && !$scope.showLoginForm();
	}

	$scope.isLoggedIn = function() {
		return $authService.isLoggedIn();
	}

	var alert = $mdDialog.alert({
	    title: 'Oops!',
	    content: 'Please check agreement before continue.',
	    ok: 'Ok!'
	});

	$scope.confirmAgreement = function()
	{
		if ( $scope.iAgree ) {
			$scope.setAgreement($scope.iAgree);
		}
		else {
			$mdDialog.show(alert);

			return false;
		}
	}
	$scope.submitSuccess = false;
	$scope.submitListing = function() {
		$scope.progress = true;
		setTimeout(function() {
			$http({
				method: "POST",
				url: '/api/request/listing',
	            headers: { 'Content-Type': undefined },
	            transformRequest: function(data) {
	            	var fd = new FormData();
		            fd.append("file", data.files[0]);
			        fd.append('model', angular.toJson($scope.listing));

			        return fd;
	            },
	            data: { model: $scope.listing, files: $scope.files }
			})
			.then(function(data) {
				$scope.progress = false;
				if (!data.data.error) {
					$scope.submitSuccess = true;
				}
			});
		}, 2000);
	}

	$scope.submitAnother = function() {
		$scope.submitSuccess = false;
		$scope.listing = {};
		window.location.reload();
		return;
	}

	$scope.getLocation = function(val) {
		return PlaceService.getSearchResult(val);
	};

	$scope.getCategory = function(val) {
		return CategoryService.searchCategory(val);
	}

	$scope.files = [];

	$scope.$on("fileSelected", function (event, args) {
        $scope.$apply(function () {
            //add the file object to the scope's files collection
            $scope.files.push(args.file);
        });
    });

}])
.directive('fileUpload', function () {
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                //iterate files since 'multiple' may be specified on the element
                for (var i = 0;i<files.length;i++) {
                    //emit event upward
                    scope.$emit("fileSelected", { file: files[i] });
                }
            });
        }
    };
});