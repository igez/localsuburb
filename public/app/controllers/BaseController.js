angular
.module('LocalSuburb.Controller.base', [])
.controller('BaseController', [
'$scope', '$authService', '$state',
function($scope, $authService, $state) {

	$scope.isLoggedIn = function() {
		return $authService.isLoggedIn();
	}

	$scope.logout = function() {
		if ($authService.logout()) {
			$scope.$broadcast( "permissionDenied" );
			$state.go('root.login');
		}
	}

}]);

