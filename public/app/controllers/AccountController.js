angular
.module('LocalSuburb.Controller.AccountController', [])
.controller('AccountController', [
'$scope', '$authService', 'localStorageService', '$state', '$log',
function($scope, $authService, localStorageService, $state, $log) {
	$scope.registerSuccess = false;
	$scope.logout = {
		status: false,
		message: ""
	}
	$scope.userLogin = function() {
		$scope.login.error = {};
		$scope.progress = true;
		$authService
		.login({email: $scope.login.email, password: $scope.login.password})
		.then(function(data) {
			if (data.data.error) {
				$scope.login.error = {
					status: 1,
					message: data.data.message
				}
			}
			else {
				var to = (localStorageService.get('b'))
							? localStorageService.get('b')
							: 'root.index';

				$state.go(to);
			}
			$scope.progress = false;
		});
	}

	$scope.$on('permissionDenied', function(event, args) {
		$scope.logout.status = 1;
		$scope.logout.message = "You need to login before submit free listing";
	});

	$scope.userRegister = function() {
		$scope.register.error = {};
		$scope.progress = true;

		if ($scope.register.password.length < 8) {
			$scope.register.error = {
				status: 1,
				message: 'Password must be minimum 8 char combination.'
			}
			$scope.progress = false;
			return false;
		}

		if ($scope.register.password != $scope.register.passwordc) {
			$scope.register.error = {
				status: 1,
				message: 'You\'re password and password confirmation doesn\'t match.'
			}
			$scope.progress = false;
			return false;
		}

		$authService
		.register($scope.register)
		.then(function(data) {
			if (data.data.error === 0) {
				$scope.registerSuccess = true;
				$scope.registerSuccessMessage = data.data.message;
			}
		});

		$log.warn($scope.register);
	}
}])