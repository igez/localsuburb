angular
.module('LocalSuburb.Controller.index', ['GoogleMapPlaceService'])
.controller('IndexController', [
	'$scope',
	'PlaceService',
    'CategoryService',
    '$state',
    '$helper',
    '$mdDialog',
	function($scope, PlaceService, CategoryService, $state, $helper, $mdDialog){
	
	// var input = document.getElementById('location');

	// var options = {
	//   	types: ['(cities)'],
	//   	componentRestrictions: {country: 'aus'}
	// };

	// autocomplete = new google.maps.places.Autocomplete(input, options);

	$scope.states = ['Bakery', 'Plumbing', 'Electrician', 'Web Designer'];

	$scope.querySearch = function(text) {
		return CategoryService.searchCategory(text);
	}

    $scope.findCategory = function(keyword) {

    }

	$scope.findCity = function(keyword) {
		return PlaceService.getSearchResult(keyword);
	}

	var alert = $mdDialog.alert({
	    title: 'Input field required',
	    content: 'Please type both category and location',
	    ok: 'Ok, I understand'
	});

	$scope.submitSearch = function(ev) {
		if ( typeof $scope.selectedItem === 'undefined' || typeof $scope.selectedPlace === 'undefined' ) {
		    $mdDialog.show(alert);
			return false;
		}

		$state.go('root.search', {
			category: $helper.slugify( $scope.selectedItem.value ),
			location: $helper.slugify( $scope.selectedPlace.value )
		});
	}

}]);
