/**
 * Search Controller
 */
angular
.module('LocalSuburb.Controller.search', ['ngAnimate'])
.controller('SearchController', [
	'$scope',
	'$stateParams',
	'$listingLocator',
	function($scope, $stateParams, $listingLocator){
	
		$scope.listings = [];

		$listingLocator.find($stateParams).then(function(data) {
			$scope.listings = data;
		});

}])