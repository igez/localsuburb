<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model {

	protected $table = 'locations';
	public $timestamps = false;

	public function Listing()
	{
		return $this->hasOne('App\Models\Listing', 'location_id', 'id');
	}

	public function setSlugAttribute($value)
	{
		$this->attributes['slug'] = $this->slugify($value);
	}

	protected function slugify($text)
	{
		$text = str_replace(', ', ',', $text);

		return strtolower( str_replace(' ', '-', $text) );
	}

}