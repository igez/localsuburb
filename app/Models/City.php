<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

	protected $table = 'citysuburbs';
	public $timestamps = false;

}
