<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Business extends Model {

	protected $table = 'business';
	public $timestamps = false;

	public function Listing()
	{
		return $this->hasOne('App\Models\Listing', 'business_id', 'id');
	}

	public function setSlugAttribute($value)
	{
		$this->attributes['slug'] = strtolower( str_replace(' ', '-', $value) );
	}

	public function setWebsiteAttribute($value) {
		$address = str_replace('http://', '', $value);

		$this->attributes['website'] = str_replace('https://', '', $value);
	}

}