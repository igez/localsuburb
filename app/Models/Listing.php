<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model {

	protected $table = 'listings';
	public $timestamps = false;

	public function Category()
	{
		return $this->belongsTo('App\Models\Category', 'category_id', 'id');
	}

	public function Location()
	{
		return $this->belongsTo('App\Models\Location', 'location_id', 'id');
	}

	public function Business()
	{
		return $this->belongsTo('App\Models\Business', 'business_id', 'id');
	}

	public function User()
	{
		return $this->belongsTo('App\User', 'user_id', 'id');
	}

}