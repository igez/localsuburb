<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * API Route Group
 */
Route::group(['prefix' => 'api'], function() {
	Route::get('/', function() {
		return response()->json('API');
	});

	Route::get('category', 'APIController@placeCategory');
	Route::get('listing', 'APIController@searchListing');

	Route::group(['prefix' => 'auth'], function() {
		Route::post('login', 'APIController@login');
		Route::get('status', 'APIController@status');
		Route::post('register', 'APIController@registerAccount');
	});

	Route::group(['prefix' => 'request'], function() {
		Route::post('listing', [
			'middleware' => 'auth',
			'uses' => 'APIController@request'
		]);
	});

	Route::group(['prefix' => 'suburb'], function() {
		Route::get('/', function() {
			if ( Input::has('search') ) {
				try {
					// DB::enableQueryLog();
					$results = DB::select(
						DB::raw("SELECT id, concat(`City`,', ',`CountryName`) as result, MATCH(`City`) AGAINST (:match) AS relevance FROM `citysuburbs` WHERE `City` LIKE :test OR `CountryName` LIKE :test2 GROUP BY `City`,`Countryname` HAVING relevance >= 0 ORDER BY relevance DESC LIMIT 10"),
						[
							'test' => '%'.Input::get('search').'%',
							'test2' => '%'.Input::get('search').'%',
							'match' => '+'.Input::get('search'),
						]
					);
					return response()->json($results);
				} catch(Exception $e) {
					return response()->json($e->getMessage());
				}

			}
		});
	});
});

Route::group(['prefix' => 'auth'], function() {
	Route::get('validate/{token}', 'APIController@validateAccount');
});

/**
 * Account Route Group
 */
Route::group(['prefix' => 'account'], function() {
	Route::get('login', [
		'as' 	=> 'Account.Login',
		'uses'	=> '\App\Http\Controllers\Admin\AccountController@login'
	]);
	Route::post('login', [
		'as' 	=> 'Account.Auth',
		'uses'	=> '\App\Http\Controllers\Admin\AccountController@auth'
	]);
	Route::get('logout', [
		'as' 	=> 'Account.Logout',
		'uses'	=> '\App\Http\Controllers\Admin\AccountController@logout'
	]);
});

/**
 * Admin Route Group
 */
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {
	Route::get('/', [
		'as' => 'Admin.Dashboard',
		'uses' => '\App\Http\Controllers\Admin\AdminController@dashboard'
	]);

	/**
	 * Listing Group
	 */
	Route::group(['prefix' => 'listing'], function() {
		Route::get('/', [
			'as' => 'Admin.Listing.Index',
			'uses' => '\App\Http\Controllers\Admin\ListingController@index'
		]);
		Route::get('pending', [
			'as' => 'Admin.Listing.Pending',
			'uses' => '\App\Http\Controllers\Admin\ListingController@pending'
		]);
		Route::any('new', [
			'as' => 'Admin.Listing.New',
			'uses' => '\App\Http\Controllers\Admin\ListingController@new_listing'
		]);
		Route::any('edit', [
			'as' => 'Admin.Listing.Edit',
			'uses' => '\App\Http\Controllers\Admin\ListingController@edit'
		]);
		Route::any('validate/{id}', [
			'as' => 'Admin.Listing.Validate',
			'uses' => '\App\Http\Controllers\Admin\ListingController@validateListing'
		]);
	});

	/**
	 * Business Route Group
	 */
	Route::group(['prefix' => 'business'], function() {
		Route::get('/', [
			'as' => 'Admin.Business.Index',
			'uses' => '\App\Http\Controllers\Admin\BusinessController@index'
		]);
		Route::get('new', [
			'as' => 'Admin.Business.New',
			'uses' => '\App\Http\Controllers\Admin\BusinessController@new_listing'
		]);
	});
});

/**
 * Pass route to angular
 */
Route::any('/', function() {   
	// dd(base_path('resources'));
    return File::get(public_path() . '/public/app.html');
});

Route::any('{path?}', function()
{
    return File::get(public_path() . '/public/app.html');
})->where("path", ".+");