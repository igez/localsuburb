<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Zofe\Rapyd\DataGrid\DataGrid;
use DataSet;
use Zofe\Rapyd\DataForm\DataForm;
use Zofe\Rapyd\DataEdit\DataEdit;

use App\Models\Listing;
use App\Models\Category;
use App\Models\Location;
use App\Models\Business;

use Carbon\Carbon;

class ListingController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$grid = DataGrid::source(Listing::with(['category', 'business', 'location'])->wherePending(0));  //same source types of DataSet

		$grid->add('business.name','Business Name');
		$grid->add('location.location','Location');
		$grid->add('category.category','Category'); 
		
		$grid->edit('/admin/listing/edit', 'Edit','modify|delete'); //shortcut to link DataEdit
		$grid->link('/admin/listing/new',"Add New", "TR");
		$grid->link('/admin/listing/pending',"Pending", "TR");
		// $grid->orderBy('article_id','desc'); //default orderby
		$grid->paginate(10); //pagination

		return view('admin.listing.index', compact('grid', 'cc'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function new_listing()
	{
		$extension = null;
		$name = null;
		if ( \Input::hasFile('photo') ) {
	    	$extension = \Input::file('photo')->getClientOriginalExtension();
	    	$name = \Input::get('location') . \Input::get('name') . \Input::file('photo')->getClientOriginalName();
	    }

		$form = DataForm::create();

		$form->add('business.name', 'Business Name', 'text')->rule('required');
		$form->add('email', 'Email', 'text');
		$form->add('category.category', 'Category', 'select')
			->options(Category::lists('Category', 'id'))
			->rule('required');
		$form->add('business.description', 'Description', 'textarea')->rule('required');
		$form->add('location.location', 'Location', 'text')->rule('required');
		$form->add('business.address', 'Address', 'text')->rule('required')->placeholder('Without Suburb/City/Country please');
		$form->add('business.phone', 'Phone', 'text');
		$form->add('business.website', 'Website', 'text')->placeholder('www.localsuburb.com (Without http:// please)');
		$form->add('business.photo','Photo', 'image')
			->rule('mimes:jpeg,jpg,png,gif')
			->move('public/uploads/images/', SHA1($name) . '.' . $extension)
			->preview(80,80)
			->fit(120, 120);
		$form->submit('Save');

		$form->passed(function() use ($form)
		{
		    extract(\Input::all());


		    if ( \Input::hasFile('photo') ) {
		    	$filename = \Input::get('location') . \Input::get('name') . \Input::file('photo')->getClientOriginalName();
		    }

		    $business = new Business;
		    $business->slug = $name;
		    $business->name = $name;
		    $business->email = $email;
		    $business->address = $address;
		    $business->phone = $phone;
		    $business->website = $website;
		    $business->description = $description;
		    $business->photo = (\Input::hasFile('photo'))
		    	? 'uploads/images/' . SHA1($filename) . '.' . \Input::file('photo')->getClientOriginalExtension() 
		    	: 'http://placehold.it/120x120';
		    $business->save();
		    
		    $locations = new Location;
		    if ( !Location::where('location', $location)->count() )
		    {
		    	$locations->slug = $location;
			    $locations->location = $location;
			    $locations->save();
		    }
		    else {
		    	$locations = Location::where('location', $location)->first();
		    }
			    

		    $listing = new Listing;
		    $listing->location_id = $locations->id;
		    $listing->business_id = $business->id;
		    $listing->category_id = $category;
		    $listing->created_at = Carbon::now();

		    $listing->save();

	    	$form->message("Listing Saved");
			$form->link("/admin/listing", "Ok!");

		});

		return view('admin.listing.new', compact('form'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function pending()
	{
		$page_title = 'Pending Listing';
		$dataset = DataSet::source(
			Listing::with([
				'category', 'business', 'location', 'user'
			])->wherePending(1)
		)
		->paginate(10)
		->getSet();

		return view('admin.listing.pending', compact('dataset', 'page_title'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Listing $listing)
	{
		$edit = DataEdit::source($listing);

		$edit->link("/admin/listing","Back to Listing", "TR")->back();

		return view('admin.listing.edit', compact('edit'));
	}


	public function validateListing($id) {
		$listing = Listing::find($id);
		if ( $listing ) {
			$listing->pending = 0;

			if ($listing->save()) {
				return redirect()->back()->with('message', 'Listing id ' . $id . ' has been validated.');
			}
			else {
				return redirect()->back()->with('error', 'Oops! Something went wrong!');
			}
		}
		else {
			return redirect()->back()->with('error', 'undefined listing id '. $id);
		}
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	protected function slugify($text)
	{
		$text = str_replace(' ', '_', $text);

		return str_replace(':', '', $text);
	}

}
