<?php namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Listing;
use App\Models\Business;
use App\Models\Location;
use App\User;
use Auth;
use Input;
use DB;

use Mail;

use Intervention\Image\ImageManagerStatic as Image;

class APIController extends Controller {

	public function __construct(\Illuminate\Http\Request $request)
	{
		$this->request = $request;
	}

	public function login(User $user) {
		if ( !Input::has('email') || !Input::has('password') ) {
			return response()->json([
				'error' => 1,
				'message' => 'Invalid request'
			]);
		}
		if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')])) {
			return response()->json([
				'error' => 0,
				'metadata' => json_encode(['email' => Auth::user()->email, 'name' => Auth::user()->name]),
				'token' => bcrypt(Input::get('email') . Input::get('password'))
			]);
		}
		else {
			return response()->json([
				'error' => 1,
				'message' => 'Invalid email or password.'
			]);
		}
	}

	public function placeCategory(Category $category)
	{
		$results = ( $this->request->has('s') ) 
			? $category->where('category', 'like', '%' . $this->request->input('s') . '%')
			: $category;

		return response()->json([
			'count' 	=> $results->count(),
			'results' 	=> $results->orderBy('category', 'ASC')->take(20)->get()
		]);
	}

	public function searchListing(Category $category, Listing $listing)
	{
		if ( $this->request->has('location') && $this->request->has('type') )
		{
			$results = $listing->whereHas('Location', function($q) {
				$q->where('slug', 'like', '%' . strtolower($this->request->input('location')) . '%');
			});

			$type = $this->request->input('type');

			$results->whereHas('Category', function($q) use($type) {
				$q->where('slug', 'like', strtolower($type));
			});

			return response()->json([
				'error'		=> 0,
				'count'		=> $results->count(),
				'results'	=> $results->wherePending(0)->with(['Location', 'Business', 'Category'])->get()
			]);
		}
		else 
		{
			return response()->json([
				'error'		=> 1,
				'message'	=> 'Invalid request'
			]);
		}
	}

	public function request(Business $business, Listing $listing, Location $elocation)
	{
		if ( Input::has('model') ) {
			$model = json_decode(Input::get('model'));
			if ( !$this->bussinessExist($model->name) ) {
				DB::beginTransaction();
				try {
					if ( Input::hasFile('file') ) {
				    	$filename = $model->location->value . $model->name . Input::file('file')->getClientOriginalName();

						$extension = Input::file('file')->getClientOriginalExtension();
						$image = Image::make(Input::file('file')->getRealPath());
						$image->fit(120, 120);
						$image->save('public/uploads/images/' . SHA1($filename) . '.' . Input::file('file')->getClientOriginalExtension(), 100);
				    }

					$business->slug = $model->name;
				    $business->name = $model->name;
				    $business->email = Auth::user()->email;
				    $business->address = $model->address;
				    $business->phone = $model->phone;
				    $business->website = (isset($model->website)) ? $model->website : NULL;
				    $business->description = $model->description;
				    $business->photo = (Input::hasFile('file'))
			    	? 'public/uploads/images/' . SHA1($filename) . '.' . Input::file('file')->getClientOriginalExtension()
			    	: 'http://placehold.it/120x120';

				    $business->save();

				    if ( !$elocation->where('location', $model->location->value)->count() )
				    {
				    	$elocation->slug = $model->location->value;
					    $elocation->location = $model->location->value;
					    $elocation->save();
				    }
				    else {
				    	$elocation = Location::where('location', $model->location->value)->first();
				    }

				    $listing->location_id = $elocation->id;
				    $listing->business_id = $business->id;
				    $listing->category_id = $model->category->key;
				    $listing->user_id = Auth::user()->id;
				    $listing->pending = 1;

				    $listing->save();

				    DB::commit();

				    return response()->json([
				    	'error' => 0,
				    	'message' => 'Your listing has been submited.'
			    	]);
				} catch (Exception $e) {
					DB::rollback();
					return response()->json([
						'error' => 1,
						'message' => 'Something went wrong with our side',
						'debug' => $e->getMessage()
					]);
				}
			}
			else {
				return response()->json([
					'error' => 1,
					'message' => "Business with name $model->name is already exist!"
				]);
			}
		}
	}

	public function bussinessExist($name) {
		return ( Business::whereName($name)->first() ) ? true : false;
	}

	public function registerAccount()
	{
		if (Input::has('email') && Input::has('password')) {
			// check if email exist ?
			if (User::whereEmail(Input::get('email'))->count())
				return response()->json([
					'error' => 1,
					'message' => sprintf("Email %s has already taken.", Input::get('email'))
				]);

			DB::beginTransaction();
			try {
				$user = new User;
				$user->email = Input::get('email');
				$user->password = bcrypt(Input::get('password'));
				$user->name = Input::get('fname') . ' ' . Input::get('lname');
				$user->token = SHA1(Input::get('email') . Input::get('password')) . md5(Input::get('email') . Input::get('password'));
				$user->validated = 0;
				$user->isAdmin = 0;

				if ($user->save()) {
					DB::commit();
					Mail::send('emails.welcome',['token' => $user->token, 'name' => $user->name], function($message) use ($user)
					{
					   	$message->from('admin@localsuburb.com', 'LocalSuburb');
						$message->subject('Welcome to LocalSubrub');
    					$message->to($user->email);
					});
					return response()->json([
						'error' => 0,
						'message' => 'Your registration success, please check your email, and follow the validation process.'
					]);
				}
			} catch(Exception $e) {
				DB::rollback();
				Log::error($e->getMessage() . ' on ' . $e->getLine());
				return response()->json([
					'error' => 1,
					'message' => 'Service returned error'
				]);
			}
		}
		else {
			return response()->json([
				'error' => 1,
				'message' => 'Invalid format'
			]);
		}
	}

	public function validateAccount($token) {

		$user = User::whereToken($token)->first();

		if ($user) {
			$user->validated = 1;
			$user->save();
			Auth::login($user);

			session(['token_validated' => 'You\'re account has been validated']);
			return redirect()->to('/login');
		}

	}

}
