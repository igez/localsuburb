<aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
          <div class="pull-left image">
              <img src="/public/app/assets/admin/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
          </div>
          <div class="pull-left info">
              <p>John Doe</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
      </div>

      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
            <a href="{{ route('Admin.Dashboard') }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>Listing</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('Admin.Listing.Index') }}"><i class="fa fa-circle-o"></i> View Listing</a></li>
                <li><a href="{{ route('Admin.Listing.New') }}"><i class="fa fa-circle-o"></i> New Listing</a></li>
                <li><a href="{{ route('Admin.Listing.Pending') }}"><i class="fa fa-circle-o"></i> Pending Listing</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="{{ route('Admin.Business.Index') }}">
                <i class="fa fa-pie-chart"></i>
                <span>Business</span>
            </a>
        </li>
    </ul>
    </section>
  <!-- /.sidebar -->
</aside>