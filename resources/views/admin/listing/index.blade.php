@extends('admin')

@section('custom_css')
	{!! Rapyd::styles() !!} 
@stop

@section('content')
	<div class="box">
		<div class="box-header">
          <h3 class="box-title">All Listing</h3>
        </div>
        <div class="box-body">
        	{!! $grid !!}
        </div>
	</div>
@stop

@section('custom_js')
	{!! Rapyd::scripts() !!}
@stop