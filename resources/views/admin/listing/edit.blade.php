@extends('admin')

@section('custom_css')
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
@stop

@section('content')

	<div class="box">
		<div class="box-body">
			<div class="row">
				<div class="col-md-8">
					{!! $edit !!}
				</div>
			</div>
		</div>
	</div>

@stop

@section('custom_js')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
<script type="text/javascript">
	(function($) {
		'use strict';

		$('#location').autocomplete({
			minLength: 2,
			source: function(request, response) {
				console.log(request);
				var service = new google.maps.places.AutocompleteService(),
					that = this;

				service.getPlacePredictions({ 
					input: request.term, 
					types: ['(regions)'], 
					componentRestrictions: {country: 'aus'}
				}, function(predictions, status) {
					var data = [];

					$.each(predictions, function(key, val) {
						data.push(val.description);
					})
					console.log(data);
					response(data);
				});
			}
		})

	})(jQuery);
</script>
@stop