@extends('admin')

@section('custom_css')
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
@stop

@section('content')

	<div class="box">
		<div class="box-body">
			<div class="row">
				<div class="col-md-8">
					{!! $form !!}
				</div>
			</div>
		</div>
	</div>

@stop

@section('custom_js')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
<script type="text/javascript">
	(function($) {
		'use strict';

		$('#location').autocomplete({
			minLength: 2,
			source: function(request, response) {
				$.ajax({
					url: '/api/suburb?search=' + request.term,
					type: 'GET',
					success: function(data) {
						var results = [];

						$.each(data, function(i, v) {
							results.push(v.result);
						})
						response(results);
					}
				})
			}
		});

		var filterCountry = function(arr) {
			var AllowedCountries = ['Australia', 'United States', 'United Kingdom', 'Ireland'],
				exist = false;

			$.each(arr, function(key, val) {
				if ( AllowedCountries.indexOf(val.value) !== -1 )
					exist = true;
			});
			return exist;
		}

	})(jQuery);
</script>
@stop