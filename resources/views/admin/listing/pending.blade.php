@extends('admin')

@section('custom_css')
	{!! Rapyd::styles() !!} 
@stop

@section('content')
	<div class="box">
		<div class="box-header">
          <h3 class="box-title">Pending Listing</h3>
        </div>
        @if (!empty(session('message')))
			<div class="alert alert-success">
				{!! session('message') !!}
			</div>
        @endif

        @if (!empty(session('error')))
			<div class="alert alert-danger">
				{!! session('error') !!}
			</div>
        @endif
        <div class="box-body">
        	<table class="table">
        		<thead>
        			<tr>
        				<th>User Name</th>
	        			<th>Business Name</th>
	        			<th>Location</th>
	        			<th>Category</th>
	        			<th>Submitted at</th>
	        			<th>Action</th>
	        		</tr>
        		</thead>
        		<tbody>
        			@foreach ($dataset->data as $k => $data)
        				<tr>
        					<td>{!! (isset($data->user)) ? $data->user->name : 'NULL' !!}</td>
        					<td>{!! $data->business->name !!}</td>
        					<td>{!! $data->location->location !!}</td>
        					<td>{!! $data->category->category !!}</td>
        					<td>{!! $data->created_at !!}</td>
        					<td><a href="{{ url('/admin/listing/validate/' . $data->id . '?token=' . csrf_token()) }}" onclick="javascript:return confirm('Are you sure ?');" class="btn btn-primary">Validate</a></td>
        				</tr>
		        	@endforeach
        		</tbody>
        	</table>
        </div>
	</div>
@stop

@section('custom_js')
	{!! Rapyd::scripts() !!}
@stop